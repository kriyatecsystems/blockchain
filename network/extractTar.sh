#!/bin/bash

## Parse mode
  MODE=$1


# Determine mode of operation and printing out what we asked for
if [ "$MODE" == "orgs" ]; then
  
    FILE=msp.org1.tar.gz
    if test -f "$FILE"; then
        mkdir -p "./organizations/peerOrganizations/org1.kriyatec.com"
        tar -xf msp.org1.tar.gz -C "./organizations/peerOrganizations/org1.kriyatec.com"
        mv "./organizations/peerOrganizations/org1.kriyatec.com/rootca/"* "./organizations/peerOrganizations/org1.kriyatec.com"
        rm -rf "./organizations/peerOrganizations/org1.kriyatec.com/rootca"
        echo "$FILE Extracted"
    fi

    
    FILE=msp.org2.tar.gz    
    if test -f "$FILE"; then
        mkdir -p "./organizations/peerOrganizations/org2.kriyatec.com"
        tar -xf msp.org2.tar.gz -C "./organizations/peerOrganizations/org2.kriyatec.com"
        mv "./organizations/peerOrganizations/org2.kriyatec.com/rootca/"* "./organizations/peerOrganizations/org2.kriyatec.com"
        rm -rf "./organizations/peerOrganizations/org2.kriyatec.com/rootca"
        echo "$FILE Extracted"
    fi

    
    FILE=msp.org3.tar.gz    
    if test -f "$FILE"; then
        mkdir -p "./organizations/peerOrganizations/org3.kriyatec.com"
        tar -xf msp.org3.tar.gz -C "./organizations/peerOrganizations/org3.kriyatec.com"
        mv "./organizations/peerOrganizations/org3.kriyatec.com/rootca/"* "./organizations/peerOrganizations/org3.kriyatec.com"
        rm -rf "./organizations/peerOrganizations/org3.kriyatec.com/rootca"
        echo "$FILE Extracted"
    fi
elif [ "$MODE" == "channel" ]; then
  FILE=channel.tar.gz    
    if test -f "$FILE"; then
       
        tar -xf channel.tar.gz
        echo "$FILE Extracted"
    fi
fi