#!/bin/bash



C_RESET='\033[0m'
C_RED='\033[0;31m'
C_GREEN='\033[0;32m'
C_BLUE='\033[0;34m'
C_YELLOW='\033[1;33m'


# infoln echos in blue color
function infoln() {
  println "${C_BLUE}${1}${C_RESET}"
}


# println echos string
function println() {
  echo -e "$1"
}


function createOrg1() {
  infoln "Enrolling the CA admin"
  mkdir -p organizations/peerOrganizations/org1.kriyatec.com/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/org1.kriyatec.com/

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-org1 --tls.certfiles "${PWD}/organizations/fabric-ca/org1/tls-cert.pem"
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-org1.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-org1.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-org1.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-org1.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/msp/config.yaml"

  infoln "Registering peer0"
  set -x
  fabric-ca-client register --caname ca-org1 --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/organizations/fabric-ca/org1/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering user"
  set -x
  fabric-ca-client register --caname ca-org1 --id.name user1 --id.secret user1pw --id.type client --tls.certfiles "${PWD}/organizations/fabric-ca/org1/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering the org admin"
  set -x
  fabric-ca-client register --caname ca-org1 --id.name org1admin --id.secret org1adminpw --id.type admin --tls.certfiles "${PWD}/organizations/fabric-ca/org1/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Generating the peer0 msp"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7054 --caname ca-org1 -M "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/msp" --csr.hosts peer0.org1.kriyatec.com --tls.certfiles "${PWD}/organizations/fabric-ca/org1/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/msp/config.yaml"

  infoln "Generating the peer0-tls certificates"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7054 --caname ca-org1 -M "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/tls" --enrollment.profile tls --csr.hosts peer0.org1.kriyatec.com --csr.hosts localhost --tls.certfiles "${PWD}/organizations/fabric-ca/org1/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/tls/ca.crt"
  cp "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/tls/signcerts/"* "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/tls/server.crt"
  cp "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/tls/keystore/"* "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/tls/server.key"

  mkdir -p "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/msp/tlscacerts"
  cp "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/tlsca"
  cp "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/tlsca/tlsca.org1.kriyatec.com-cert.pem"

  mkdir -p "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/ca"
  cp "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/peers/peer0.org1.kriyatec.com/msp/cacerts/"* "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/ca/ca.org1.kriyatec.com-cert.pem"

  infoln "Generating the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-org1 -M "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/users/User1@org1.kriyatec.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/org1/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/users/User1@org1.kriyatec.com/msp/config.yaml"

  infoln "Generating the org admin msp"
  set -x
  fabric-ca-client enroll -u https://org1admin:org1adminpw@localhost:7054 --caname ca-org1 -M "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/users/Admin@org1.kriyatec.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/org1/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org1.kriyatec.com/users/Admin@org1.kriyatec.com/msp/config.yaml"
}

function getRootCACert() {
  ORG=$1
  rm -rf rootca
  mkdir -p "rootca/msp/cacerts"
  cp "${PWD}/organizations/peerOrganizations/${ORG}.kriyatec.com/msp/config.yaml" "rootca/msp/config.yaml"
  cp "${PWD}/organizations/peerOrganizations/${ORG}.kriyatec.com/msp/cacerts/"* "rootca/msp/cacerts"
  cp "${PWD}/organizations/peerOrganizations/${ORG}.kriyatec.com/msp/IssuerPublicKey" "rootca/msp/IssuerPublicKey"
  cp "${PWD}/organizations/peerOrganizations/${ORG}.kriyatec.com/msp/IssuerRevocationPublicKey" "rootca/msp/IssuerRevocationPublicKey"
  
  mkdir -p "rootca/peers/peer0.${ORG}.kriyatec.com/tls"
  cp "${PWD}/organizations/peerOrganizations/${ORG}.kriyatec.com/peers/peer0.${ORG}.kriyatec.com/tls/ca.crt" "rootca/peers/peer0.${ORG}.kriyatec.com/tls/ca.crt"
  
  mkdir -p "rootca/users/Admin@${ORG}.kriyatec.com/msp/cacerts"
  mkdir -p "rootca/users/Admin@${ORG}.kriyatec.com/msp/signcerts"
  cp "${PWD}/organizations/peerOrganizations/${ORG}.kriyatec.com/users/Admin@${ORG}.kriyatec.com/msp/config.yaml" "rootca/users/Admin@${ORG}.kriyatec.com/msp/config.yaml"
  cp "${PWD}/organizations/peerOrganizations/${ORG}.kriyatec.com/users/Admin@${ORG}.kriyatec.com/msp/cacerts/"* "rootca/users/Admin@${ORG}.kriyatec.com/msp/cacerts"
  cp "${PWD}/organizations/peerOrganizations/${ORG}.kriyatec.com/users/Admin@${ORG}.kriyatec.com/msp/signcerts/"* "rootca/users/Admin@${ORG}.kriyatec.com/msp/signcerts"
  cp "${PWD}/organizations/peerOrganizations/${ORG}.kriyatec.com/users/Admin@${ORG}.kriyatec.com/msp/IssuerPublicKey" "rootca/users/Admin@${ORG}.kriyatec.com/msp/IssuerPublicKey"
  cp "${PWD}/organizations/peerOrganizations/${ORG}.kriyatec.com/users/Admin@${ORG}.kriyatec.com/msp/IssuerRevocationPublicKey" "rootca/users/Admin@${ORG}.kriyatec.com/msp/IssuerRevocationPublicKey"
  
  
  tar -zcvf "msp.${ORG}.tar.gz" ./rootca/
  rm -rf rootca
}

function createOrg2() {
  infoln "Enrolling the CA admin"
  mkdir -p organizations/peerOrganizations/org2.kriyatec.com/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/org2.kriyatec.com/

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:8054 --caname ca-org2 --tls.certfiles "${PWD}/organizations/fabric-ca/org2/tls-cert.pem"
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-org2.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-org2.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-org2.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-org2.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/msp/config.yaml"

  infoln "Registering peer0"
  set -x
  fabric-ca-client register --caname ca-org2 --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/organizations/fabric-ca/org2/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering user"
  set -x
  fabric-ca-client register --caname ca-org2 --id.name user1 --id.secret user1pw --id.type client --tls.certfiles "${PWD}/organizations/fabric-ca/org2/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering the org admin"
  set -x
  fabric-ca-client register --caname ca-org2 --id.name org2admin --id.secret org2adminpw --id.type admin --tls.certfiles "${PWD}/organizations/fabric-ca/org2/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Generating the peer0 msp"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:8054 --caname ca-org2 -M "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/msp" --csr.hosts peer0.org2.kriyatec.com --tls.certfiles "${PWD}/organizations/fabric-ca/org2/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/msp/config.yaml"

  infoln "Generating the peer0-tls certificates"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:8054 --caname ca-org2 -M "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/tls" --enrollment.profile tls --csr.hosts peer0.org2.kriyatec.com --csr.hosts localhost --tls.certfiles "${PWD}/organizations/fabric-ca/org2/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/tls/ca.crt"
  cp "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/tls/signcerts/"* "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/tls/server.crt"
  cp "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/tls/keystore/"* "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/tls/server.key"

  mkdir -p "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/msp/tlscacerts"
  cp "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/tlsca"
  cp "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/tlsca/tlsca.org2.kriyatec.com-cert.pem"

  mkdir -p "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/ca"
  cp "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/peers/peer0.org2.kriyatec.com/msp/cacerts/"* "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/ca/ca.org2.kriyatec.com-cert.pem"

  infoln "Generating the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:8054 --caname ca-org2 -M "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/users/User1@org2.kriyatec.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/org2/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/users/User1@org2.kriyatec.com/msp/config.yaml"

  infoln "Generating the org admin msp"
  set -x
  fabric-ca-client enroll -u https://org2admin:org2adminpw@localhost:8054 --caname ca-org2 -M "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/users/Admin@org2.kriyatec.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/org2/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org2.kriyatec.com/users/Admin@org2.kriyatec.com/msp/config.yaml"
}



function createOrg3() {
  infoln "Enrolling the CA admin"
  mkdir -p organizations/peerOrganizations/org3.kriyatec.com/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/org3.kriyatec.com/

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:10054 --caname ca-org3 --tls.certfiles "${PWD}/organizations/fabric-ca/org3/tls-cert.pem"
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-10054-ca-org3.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-10054-ca-org3.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-10054-ca-org3.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-10054-ca-org3.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/msp/config.yaml"

  infoln "Registering peer0"
  set -x
  fabric-ca-client register --caname ca-org3 --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles "${PWD}/organizations/fabric-ca/org3/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering user"
  set -x
  fabric-ca-client register --caname ca-org3 --id.name user1 --id.secret user1pw --id.type client --tls.certfiles "${PWD}/organizations/fabric-ca/org3/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering the org admin"
  set -x
  fabric-ca-client register --caname ca-org3 --id.name org3admin --id.secret org3adminpw --id.type admin --tls.certfiles "${PWD}/organizations/fabric-ca/org3/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Generating the peer0 msp"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:10054 --caname ca-org3 -M "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/msp" --csr.hosts peer0.org3.kriyatec.com --tls.certfiles "${PWD}/organizations/fabric-ca/org3/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/msp/config.yaml"

  infoln "Generating the peer0-tls certificates"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:10054 --caname ca-org3 -M "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/tls" --enrollment.profile tls --csr.hosts peer0.org3.kriyatec.com --csr.hosts localhost --tls.certfiles "${PWD}/organizations/fabric-ca/org3/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/tls/ca.crt"
  cp "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/tls/signcerts/"* "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/tls/server.crt"
  cp "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/tls/keystore/"* "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/tls/server.key"

  mkdir -p "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/msp/tlscacerts"
  cp "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/tlsca"
  cp "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/tlsca/tlsca.org3.kriyatec.com-cert.pem"

  mkdir -p "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/ca"
  cp "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/peers/peer0.org3.kriyatec.com/msp/cacerts/"* "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/ca/ca.org3.kriyatec.com-cert.pem"

  infoln "Generating the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:10054 --caname ca-org3 -M "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/users/User1@org3.kriyatec.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/org3/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/users/User1@org3.kriyatec.com/msp/config.yaml"

  infoln "Generating the org admin msp"
  set -x
  fabric-ca-client enroll -u https://org3admin:org3adminpw@localhost:10054 --caname ca-org3 -M "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/users/Admin@org3.kriyatec.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/org3/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/msp/config.yaml" "${PWD}/organizations/peerOrganizations/org3.kriyatec.com/users/Admin@org3.kriyatec.com/msp/config.yaml"
}

function createOrderer() {
  infoln "Enrolling the CA admin"
  mkdir -p organizations/ordererOrganizations/kriyatec.com

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/kriyatec.com

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:9054 --caname ca-orderer --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/organizations/ordererOrganizations/kriyatec.com/msp/config.yaml"

  infoln "Registering orderer"
  set -x
  fabric-ca-client register --caname ca-orderer --id.name orderer --id.secret ordererpw --id.type orderer --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering the orderer admin"
  set -x
  fabric-ca-client register --caname ca-orderer --id.name ordererAdmin --id.secret ordererAdminpw --id.type admin --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Generating the orderer msp"
  set -x
  fabric-ca-client enroll -u https://orderer:ordererpw@localhost:9054 --caname ca-orderer -M "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/msp" --csr.hosts orderer.kriyatec.com --csr.hosts localhost --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/ordererOrganizations/kriyatec.com/msp/config.yaml" "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/msp/config.yaml"

  infoln "Generating the orderer-tls certificates"
  set -x
  fabric-ca-client enroll -u https://orderer:ordererpw@localhost:9054 --caname ca-orderer -M "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/tls" --enrollment.profile tls --csr.hosts orderer.kriyatec.com --csr.hosts localhost --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/tls/ca.crt"
  cp "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/tls/signcerts/"* "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/tls/server.crt"
  cp "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/tls/keystore/"* "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/tls/server.key"

  mkdir -p "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/msp/tlscacerts"
  cp "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/msp/tlscacerts/tlsca.kriyatec.com-cert.pem"

  mkdir -p "${PWD}/organizations/ordererOrganizations/kriyatec.com/msp/tlscacerts"
  cp "${PWD}/organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/tls/tlscacerts/"* "${PWD}/organizations/ordererOrganizations/kriyatec.com/msp/tlscacerts/tlsca.kriyatec.com-cert.pem"

  infoln "Generating the admin msp"
  set -x
  fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@localhost:9054 --caname ca-orderer -M "${PWD}/organizations/ordererOrganizations/kriyatec.com/users/Admin@kriyatec.com/msp" --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/ordererOrganizations/kriyatec.com/msp/config.yaml" "${PWD}/organizations/ordererOrganizations/kriyatec.com/users/Admin@kriyatec.com/msp/config.yaml"
}


## Parse mode
  MODE=$1


# Determine mode of operation and printing out what we asked for
if [ "$MODE" == "org1" ]; then
  createOrg1
  getRootCACert "org1"
elif [ "$MODE" == "org2" ]; then
  createOrg2
  getRootCACert "org2"
elif [ "$MODE" == "org3" ]; then
  createOrg3
  getRootCACert "org3"
elif [ "$MODE" == "orderer" ]; then
  createOrderer
fi