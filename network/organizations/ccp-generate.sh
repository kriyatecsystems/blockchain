#!/bin/bash

# Print the usage message
function printHelp() {
    USAGE="$1"
    println "Usage: "
    println "  ccp-generate.sh <org1 | org2 | org3>"
}

function println() {
  echo -e "$1"
}

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        organizations/ccp-template.json
}

function yaml_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        organizations/ccp-template.yaml | sed -e $'s/\\\\n/\\\n          /g'
}

## Parse mode
if [[ $# -lt 1 ]] ; then
  printHelp
  exit 0
else
  MODE=$1

  if [ "${MODE}" == "org1" ]; then
    ORG=1
    P0PORT=7051
    CAPORT=7054
    PEERPEM=organizations/peerOrganizations/org1.kriyatec.com/tlsca/tlsca.org1.kriyatec.com-cert.pem
    CAPEM=organizations/peerOrganizations/org1.kriyatec.com/ca/ca.org1.kriyatec.com-cert.pem

    echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM)" > organizations/peerOrganizations/org1.kriyatec.com/connection-org1.json
    echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM)" > organizations/peerOrganizations/org1.kriyatec.com/connection-org1.yaml

  elif [ "${MODE}" == "org2" ]; then
    ORG=2
    P0PORT=9051
    CAPORT=8054
    PEERPEM=organizations/peerOrganizations/org2.kriyatec.com/tlsca/tlsca.org2.kriyatec.com-cert.pem
    CAPEM=organizations/peerOrganizations/org2.kriyatec.com/ca/ca.org2.kriyatec.com-cert.pem

    echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM)" > organizations/peerOrganizations/org2.kriyatec.com/connection-org2.json
    echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM)" > organizations/peerOrganizations/org2.kriyatec.com/connection-org2.yaml

  elif [ "${MODE}" == "org3" ]; then
    ORG=3
    P0PORT=10051
    CAPORT=10054
    PEERPEM=organizations/peerOrganizations/org3.kriyatec.com/tlsca/tlsca.org3.kriyatec.com-cert.pem
    CAPEM=organizations/peerOrganizations/org3.kriyatec.com/ca/ca.org3.kriyatec.com-cert.pem

    echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM)" > organizations/peerOrganizations/org3.kriyatec.com/connection-org3.json
    echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM)" > organizations/peerOrganizations/org3.kriyatec.com/connection-org3.yaml

  else 
    printHelp
    exit 0
  fi
  shift
fi