#!/bin/bash

git clean -fd

git reset --hard

docker system prune -f


docker volume prune -f