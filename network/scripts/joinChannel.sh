#!/bin/bash
export FABRIC_CFG_PATH=${PWD}/configtx


CHANNEL_NAME="$1"
OrgId="$2"

. scripts/channelUtils.sh
peer version > /dev/null 2>&1

# Join all the peers to the channel
infoln "Joining org1 peer to the channel..."
joinChannel ${OrgId}
