#!/bin/bash
export FABRIC_CFG_PATH=${PWD}/configtx


CHANNEL_NAME="$1"
DELAY="$2"
MAX_RETRY="$3"
VERBOSE="$4"
: ${CHANNEL_NAME:="ktchannel"}
: ${DELAY:="3"}
: ${MAX_RETRY:="5"}
: ${VERBOSE:="false"}


. scripts/channelUtils.sh


if [ ! -d "channel-artifacts" ]; then
	mkdir channel-artifacts
fi

FABRIC_CFG_PATH=${PWD}/configtx
## Create channel genesis block
infoln "Generating channel genesis block '${CHANNEL_NAME}.block'"
createChannelGenesisBlock

FABRIC_CFG_PATH=${PWD}/configtx
BLOCKFILE="./channel-artifacts/${CHANNEL_NAME}.block"

## Create channel
infoln "Creating channel ${CHANNEL_NAME}"
createChannel
successln "Channel '$CHANNEL_NAME' created"


tar -zcvf "channel.tar.gz" ./channel-artifacts/ ./organizations/ordererOrganizations/kriyatec.com/orderers/orderer.kriyatec.com/msp/tlscacerts/tlsca.kriyatec.com-cert.pem

