# Hyperledger Fabric - Blockchain Demo (3 Orgs Network)

## Prerequisites

* Ubuntu (18.04.6 or higher)
* ­Docker
* Docker-compose
* cURL
* Golang
* ­Git
* ­IDE (VSCode recommended)
* ­Fabric Binaries
	* Install using command  (and add to PATH or copy to /usr/local/bin)

	```
	curl -sSL https://bit.ly/2ysbOFE | bash -s -- 2.3.3 1.5.2 -s -d
	```
	

## Docker Swarm Init
#### Init Swarm in Orderer VM/PC
```
docker swarm init
docker swarm join-token manager
``` 
Join Org1, Org2 & Org3 as manager for the docker cluster
```
docker swarm join --token <....> ...
```

## Label Docker Nodes

Label the docker nodes to match for constraints during Docker service placement

Get node list 
```
docker node ls
```

```
docker node update --label-add type=orderer <order_node_id>

docker node update --label-add type=org1 <org1_node_id>

docker node update --label-add type=org2 <org2_node_id>

docker node update --label-add type=org3 <org3_node_id>
```

* Verify by Running and make sure Hostname and ID matches
```
docker node ls -q | xargs docker node inspect   -f '{{ .ID }} [{{ .Description.Hostname }}]: {{ .Spec.Labels }}'
```

## Clean Artifacts / Credentials

`cd network`
######  Remove existing services from stack if already running 
```
docker stack rm kt_net
```
wait for above command to complete - check by running `docker ps -a`
##### Clean Workspace (Remove files / directories and docker cache)
```
./clean.sh
```

## Create Docker Network

```
docker network create --attachable --driver overlay kt_net
```
## CA Setup

#### Orderer
```
. ../setEnv.sh && docker stack deploy --compose-file docker/ordererOrg/docker-compose-ca.yaml kt_net
```
#### Org1
```
. ../setEnv.sh && docker stack deploy --compose-file docker/org1/docker-compose-ca.yaml kt_net
```
#### Org2
```
. ../setEnv.sh && docker stack deploy --compose-file docker/org2/docker-compose-ca.yaml kt_net
```
#### Org3
```
. ../setEnv.sh && docker stack deploy --compose-file docker/org3/docker-compose-ca.yaml kt_net
```

## Enroll Fabric CA

#### Orderer
```
. ./organizations/fabric-ca/registerEnroll.sh  orderer
```
#### Org1
```
. ./organizations/fabric-ca/registerEnroll.sh  org1
```
#### Org2
```
. ./organizations/fabric-ca/registerEnroll.sh  org2
```
#### Org3
```
. ./organizations/fabric-ca/registerEnroll.sh  org3
```
Check for ca certifcates generated in `~/networks/organizations/...` directory


## Copy Root CA/TLS certs

`msp.org1.tar.gz`, `msp.org2.tar.gz` and `msp.org3.tar.gz` files would be generated in Org1, Org2 and Org3.
Download and copy into Orderer, Org1, Org2 & Org3 VM/PC

### Extract in all VM/PC

```
./extractTar.sh orgs
```

## Generate Genisis file in Orderer VM/PC

```
export FABRIC_CFG_PATH=${PWD}/configtx
configtxgen -profile ThreeOrgsApplicationGenesis -channelID system-channel -outputBlock ./system-genesis-block/genesis.block
```
Check `~/network/system-genesis-block` directory for generated genesis file

## ORDERER / PEER / Cli Setup
Start Orderer in Orderer and Peer / CouchDB node in Org1 & Org2

#### Orderer
```
. ../setEnv.sh && docker stack deploy --compose-file docker/ordererOrg/docker-compose-net.yaml kt_net
```
#### Org1
```
. ../setEnv.sh && docker stack deploy --compose-file docker/org1/docker-compose-net.yaml kt_net
```
#### Org2
```
. ../setEnv.sh && docker stack deploy --compose-file docker/org2/docker-compose-net.yaml kt_net
```
#### Org3
```
. ../setEnv.sh && docker stack deploy --compose-file docker/org3/docker-compose-net.yaml kt_net
```
Check running services using command `docker stack services kt_net` or `docker service ls`
## Channel Setup

#### Orderer - Create Channel
```
./scripts/createChannel.sh ktchannel
```
`channel.tar.gz` is generated with channel and orderer tlsca certs
Download and copy file to Org1, Org2 & Org3 VM/PC

### Extract Channel Artifact in All 3 Orgs VM/PC
```
./extractTar.sh channel
```

#### Join Channel
#### Org1
```
./scripts/joinChannel.sh ktchannel 1
```
#### Org2
```
./scripts/joinChannel.sh ktchannel 2
```
#### Org3
```
./scripts/joinChannel.sh ktchannel 3
```

#### Set Anchor Peer
Execute script inside org cli container (Fabric Tools)
#### Org1
```
docker exec $(docker ps -q --filter name=kt_net_cli_org1) ./scripts/setAnchorPeer.sh ktchannel 1
```
#### Org2
```
docker exec $(docker ps -q --filter name=kt_net_cli_org2) ./scripts/setAnchorPeer.sh ktchannel 2
```
#### Org3
```
docker exec $(docker ps -q --filter name=kt_net_cli_org3) ./scripts/setAnchorPeer.sh ktchannel 3
```
## Deploy Chaincode
#### Connect to Org1 / Org2 / Org3 Cli (Fabric Tools)
```
docker exec -it $(docker ps -q --filter name=kt_net_cli_org1) bash
```
```
docker exec -it $(docker ps -q --filter name=kt_net_cli_org2) bash
```
```
docker exec -it $(docker ps -q --filter name=kt_net_cli_org3) bash
```
#### Pack and Install ChainCode
```
peer lifecycle chaincode package fabcar.tar.gz --path /opt/gopath/src/github.com/chaincode/fabcar/ --lang golang --label fabcar_1.0
```
```
peer lifecycle chaincode install fabcar.tar.gz
```

#### Check Commit Readiness
```
peer lifecycle chaincode checkcommitreadiness --channelID ktchannel --name fabcar --version 1.0 --sequence 1 --signature-policy "OR ('Org1MSP.peer','Org2MSP.peer','Org3MSP.peer')" --init-required --output json
```

#### Get Package ID
```
peer lifecycle chaincode queryinstalled >&log.txt
export PACKAGE_ID=$(sed -n "/fabcar_1.0/{s/^Package ID: //; s/, Label:.*$//; p;}" log.txt)
echo $PACKAGE_ID
```
#### Approve for Organisation (Run in all 3 Orgs)
```
peer lifecycle chaincode approveformyorg -o orderer.kriyatec.com:7050 --ordererTLSHostnameOverride orderer.kriyatec.com --tls --cafile "$ORDERER_CA" --channelID ktchannel --name fabcar --version 1.0 --package-id ${PACKAGE_ID} --sequence 1 --signature-policy "OR ('Org1MSP.peer','Org2MSP.peer','Org3MSP.peer')" --init-required
```
#### Check Back Commit Readiness
```
peer lifecycle chaincode checkcommitreadiness --channelID ktchannel --name fabcar --version 1.0 --sequence 1 --signature-policy "OR ('Org1MSP.peer','Org2MSP.peer','Org3MSP.peer')" --init-required --output json
```

#### Commit Chaincode (In Any of Orgs)
```
peer lifecycle chaincode commit -o orderer.kriyatec.com:7050 --ordererTLSHostnameOverride orderer.kriyatec.com --tls --cafile "$ORDERER_CA" --channelID ktchannel --name fabcar --peerAddresses peer0.org1.kriyatec.com:7051 --tlsRootCertFiles "$ORG1_PEER0_TLS_ROOTCERT_FILE"  --peerAddresses peer0.org2.kriyatec.com:9051 --tlsRootCertFiles "$ORG2_PEER0_TLS_ROOTCERT_FILE" --peerAddresses peer0.org3.kriyatec.com:10051 --tlsRootCertFiles "$ORG3_PEER0_TLS_ROOTCERT_FILE" --version 1.0 --sequence 1 --init-required --signature-policy "OR ('Org1MSP.peer','Org2MSP.peer','Org3MSP.peer')"
```

#### Query Committed
```
peer lifecycle chaincode querycommitted --channelID ktchannel
```

#### Invoke chaincode init
```
peer chaincode invoke -o orderer.kriyatec.com:7050 --ordererTLSHostnameOverride orderer.kriyatec.com --tls --cafile "$ORDERER_CA" -C ktchannel -n fabcar --isInit -c '{"function":"'initLedger'","Args":[]}'
```

#### Query Result
```
peer chaincode query -C ktchannel -n fabcar -c '{"Args":["queryAllCars"]}'
```

#### Invoke Transactions
```
peer chaincode invoke -C ktchannel -n fabcar -c '{"Args":["createCar", "CAR12345", "Honda", "Accord", "black", "Tom"]}' --tls --cafile "$ORDERER_CA"
```
```
peer chaincode query -C ktchannel -n fabcar -c '{"Args":["queryCar","CAR12345"]}'
```

#### Generate connection profile (for use by Application)
##### Org1
```
./organizations/ccp-generate.sh org1
```
##### Org2
```
./organizations/ccp-generate.sh org2
```
##### Org3
```
./organizations/ccp-generate.sh org3
```